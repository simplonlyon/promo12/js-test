/// <reference types="Cypress" />


describe('SmartBin Interface test', ()=> {

    it('should display the empty bin', () => {

        cy.visit('http://localhost/js-test');

        cy.get('p').should('have.length', 4);
        cy.get('p').first().should('contain.text', 'Grey : 0 gr');

        cy.get('#weight').type('10');

    });

})