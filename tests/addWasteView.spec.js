/// <reference types="Cypress" />

describe("Waste form Page", () => {
    
    it("Should add little glass waste on ViewBin", () => {
        // simule ce que fait l'utilisateur
        cy.visit("http://localhost/SimplonP12/Back/JS/js-test/"); // URL de notre projet dans server apache (localhost)

        // Actions utilisateur
        cy.get("select").select("glass");
        //cy.get("#waste_recyclable").click();
        cy.get("#waste_recyclable").check();
        cy.get("#waste_volume").type("1");
        cy.get("#waste_weight").type("4");

        cy.get("#submit_waste").click();

        // Vérification Front du résultat
        cy.get("#easyTitleFinder").contains("Actual Volume : 1");
        cy.get("#glass_weight").contains("Glass : 4 gr");
    });

    // it("", () => {

    // });
});