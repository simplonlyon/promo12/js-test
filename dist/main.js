/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _recycle_ViewBin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./recycle/ViewBin */ "./src/recycle/ViewBin.js");



let view = new _recycle_ViewBin__WEBPACK_IMPORTED_MODULE_0__["ViewBin"]('#target', 10, 'blue');

view.draw();

/***/ }),

/***/ "./src/recycle/SmartBin.js":
/*!*********************************!*\
  !*** ./src/recycle/SmartBin.js ***!
  \*********************************/
/*! exports provided: SmartBin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartBin", function() { return SmartBin; });
/* harmony import */ var _Waste__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Waste */ "./src/recycle/Waste.js");


class SmartBin {
    /**
     * @param {number} volume le volume max de la poubelle
     * @param {string} color la couleur esthétique de la poubelle
     * @param {Waste[]} content le contenu de la poubelle
     */
    constructor(volume, color, content = []) {
        this.volume = volume;
        this.color = color;
        this.content = content;
    }
    /**
     * Méthode pour vider la poubelle
     */
    empty() {
        this.content = [];
    }
    /**
     * Méthode pour mettre un déchet à la poubelle
     * @param {Waste} waste le déchet à mettre à la poubelle
     */
    add(waste) {
        if (waste.volume + this.totalVolume() <= this.volume) {
            this.content.push(waste);
        }
    }

    /**
     * Méthode qui calcul le volume actuel de tous les déchets contenus
     * dans la poubelle
     * @returns {number} le volume total
     */
    totalVolume() {
        let actualVolume = 0;
        for (const iterator of this.content) {
            actualVolume += iterator.volume;
        }
        return actualVolume;
        // return this.content.reduce((acc, cur) => acc+= cur.volume, 0);
    }

    /**
     * Méthode qui nous indique le poid des différents types de déchets
     * @returns {object} le bilan de ce qu'on a dans la poubelle
     */
    total() {
        let grey = 0;
        let yellow = 0;
        let green = 0;
        let glass = 0;
        for (const iterator of this.content) {
            if(!iterator.recyclable && iterator.type !== 'organic') {
                grey += iterator.weight;    
            }
            if(iterator.recyclable && (iterator.type === 'paper' || iterator.type === 'plastic')) {
                yellow += iterator.weight;
            }
            if(iterator.type === 'organic') {
                green += iterator.weight;
            }
            if(iterator.type === 'glass' && iterator.recyclable) {
                glass += iterator.weight;
            }
        }
        return {grey, yellow, green, glass};
    }
}

/***/ }),

/***/ "./src/recycle/ViewBin.js":
/*!********************************!*\
  !*** ./src/recycle/ViewBin.js ***!
  \********************************/
/*! exports provided: ViewBin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewBin", function() { return ViewBin; });
/* harmony import */ var _SmartBin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SmartBin */ "./src/recycle/SmartBin.js");



class ViewBin {
    /**
     * 
     * @param {string} selector Sélécteur css de l'élément où mettre l'affichage
     * @param {number} volume le volume de la poubelle
     * @param {string} color  la couleur de la poubelle
     */
    constructor(selector, volume, color) {
        this.selector = selector;
        this.smartBin = new _SmartBin__WEBPACK_IMPORTED_MODULE_0__["SmartBin"](volume, color);
    }

    /**
     * Méthode qui génère le html de la poubelle en propriété
     * @returns {Element} L'élément html généré
     */
    draw() {
        let target = document.querySelector(this.selector);
        target.innerHTML = '';

        let element = document.createElement('div');
        element.classList.add('smart-bin');
        element.textContent = 'Actual Volume : '+this.smartBin.totalVolume()
        this.drawWeight(element);
        element.style.backgroundColor = this.color;
        target.appendChild(element);

        return element;
    }

    /**
     * Méthode qui génère le html représentant le poid des déchets
     * @param {Element} element L'élément html où append le poid des déchets
     */
    drawWeight(element) {
        let total = this.smartBin.total();
        let pGrey = document.createElement('p');
        pGrey.textContent = 'Grey : '+ total.grey + ' gr';
        element.appendChild(pGrey);

        let pGlass = document.createElement('p');
        pGlass.textContent = 'Glass : '+ total.glass + ' gr';
        element.appendChild(pGlass);

        let pGreen = document.createElement('p');
        pGreen.textContent = 'Green : '+ total.green + ' gr';
        element.appendChild(pGreen);

        let pYellow = document.createElement('p');
        pYellow.textContent = 'Yellow : '+ total.yellow + ' gr';
        element.appendChild(pYellow);
    }

}

/***/ }),

/***/ "./src/recycle/Waste.js":
/*!******************************!*\
  !*** ./src/recycle/Waste.js ***!
  \******************************/
/*! exports provided: Waste */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Waste", function() { return Waste; });
class Waste {
    /**
     * @param {number} weight le poid du déchet
     * @param {number} volume le volume que prend le déchet
     * @param {string} type la matière du déchet (plastic, glass, organic, other, paper)
     * @param {boolean} recyclable si le déchet est recyclable ou pas
     */
    constructor(weight, volume, type, recyclable) {
        this.weight = weight;
        this.type = type;
        this.volume = volume;
        this.recyclable = recyclable;
    }
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9yZWN5Y2xlL1NtYXJ0QmluLmpzIiwid2VicGFjazovLy8uL3NyYy9yZWN5Y2xlL1ZpZXdCaW4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JlY3ljbGUvV2FzdGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQTRDOzs7QUFHNUMsZUFBZSx3REFBTzs7QUFFdEIsWTs7Ozs7Ozs7Ozs7O0FDTEE7QUFBQTtBQUFBO0FBQWdDOztBQUV6QjtBQUNQO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0EsQzs7Ozs7Ozs7Ozs7O0FDcEVBO0FBQUE7QUFBQTtBQUFzQzs7O0FBRy9CO0FBQ1A7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QixrREFBUTtBQUNwQzs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLFFBQVE7QUFDekI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDOzs7Ozs7Ozs7Ozs7QUN4REE7QUFBQTtBQUFPO0FBQ1A7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IFZpZXdCaW4gfSBmcm9tIFwiLi9yZWN5Y2xlL1ZpZXdCaW5cIjtcblxuXG5sZXQgdmlldyA9IG5ldyBWaWV3QmluKCcjdGFyZ2V0JywgMTAsICdibHVlJyk7XG5cbnZpZXcuZHJhdygpOyIsImltcG9ydCB7IFdhc3RlIH0gZnJvbSBcIi4vV2FzdGVcIjtcblxuZXhwb3J0IGNsYXNzIFNtYXJ0QmluIHtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gdm9sdW1lIGxlIHZvbHVtZSBtYXggZGUgbGEgcG91YmVsbGVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gY29sb3IgbGEgY291bGV1ciBlc3Row6l0aXF1ZSBkZSBsYSBwb3ViZWxsZVxuICAgICAqIEBwYXJhbSB7V2FzdGVbXX0gY29udGVudCBsZSBjb250ZW51IGRlIGxhIHBvdWJlbGxlXG4gICAgICovXG4gICAgY29uc3RydWN0b3Iodm9sdW1lLCBjb2xvciwgY29udGVudCA9IFtdKSB7XG4gICAgICAgIHRoaXMudm9sdW1lID0gdm9sdW1lO1xuICAgICAgICB0aGlzLmNvbG9yID0gY29sb3I7XG4gICAgICAgIHRoaXMuY29udGVudCA9IGNvbnRlbnQ7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHBvdXIgdmlkZXIgbGEgcG91YmVsbGVcbiAgICAgKi9cbiAgICBlbXB0eSgpIHtcbiAgICAgICAgdGhpcy5jb250ZW50ID0gW107XG4gICAgfVxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHBvdXIgbWV0dHJlIHVuIGTDqWNoZXQgw6AgbGEgcG91YmVsbGVcbiAgICAgKiBAcGFyYW0ge1dhc3RlfSB3YXN0ZSBsZSBkw6ljaGV0IMOgIG1ldHRyZSDDoCBsYSBwb3ViZWxsZVxuICAgICAqL1xuICAgIGFkZCh3YXN0ZSkge1xuICAgICAgICBpZiAod2FzdGUudm9sdW1lICsgdGhpcy50b3RhbFZvbHVtZSgpIDw9IHRoaXMudm9sdW1lKSB7XG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQucHVzaCh3YXN0ZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgY2FsY3VsIGxlIHZvbHVtZSBhY3R1ZWwgZGUgdG91cyBsZXMgZMOpY2hldHMgY29udGVudXNcbiAgICAgKiBkYW5zIGxhIHBvdWJlbGxlXG4gICAgICogQHJldHVybnMge251bWJlcn0gbGUgdm9sdW1lIHRvdGFsXG4gICAgICovXG4gICAgdG90YWxWb2x1bWUoKSB7XG4gICAgICAgIGxldCBhY3R1YWxWb2x1bWUgPSAwO1xuICAgICAgICBmb3IgKGNvbnN0IGl0ZXJhdG9yIG9mIHRoaXMuY29udGVudCkge1xuICAgICAgICAgICAgYWN0dWFsVm9sdW1lICs9IGl0ZXJhdG9yLnZvbHVtZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gYWN0dWFsVm9sdW1lO1xuICAgICAgICAvLyByZXR1cm4gdGhpcy5jb250ZW50LnJlZHVjZSgoYWNjLCBjdXIpID0+IGFjYys9IGN1ci52b2x1bWUsIDApO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHF1aSBub3VzIGluZGlxdWUgbGUgcG9pZCBkZXMgZGlmZsOpcmVudHMgdHlwZXMgZGUgZMOpY2hldHNcbiAgICAgKiBAcmV0dXJucyB7b2JqZWN0fSBsZSBiaWxhbiBkZSBjZSBxdSdvbiBhIGRhbnMgbGEgcG91YmVsbGVcbiAgICAgKi9cbiAgICB0b3RhbCgpIHtcbiAgICAgICAgbGV0IGdyZXkgPSAwO1xuICAgICAgICBsZXQgeWVsbG93ID0gMDtcbiAgICAgICAgbGV0IGdyZWVuID0gMDtcbiAgICAgICAgbGV0IGdsYXNzID0gMDtcbiAgICAgICAgZm9yIChjb25zdCBpdGVyYXRvciBvZiB0aGlzLmNvbnRlbnQpIHtcbiAgICAgICAgICAgIGlmKCFpdGVyYXRvci5yZWN5Y2xhYmxlICYmIGl0ZXJhdG9yLnR5cGUgIT09ICdvcmdhbmljJykge1xuICAgICAgICAgICAgICAgIGdyZXkgKz0gaXRlcmF0b3Iud2VpZ2h0OyAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGl0ZXJhdG9yLnJlY3ljbGFibGUgJiYgKGl0ZXJhdG9yLnR5cGUgPT09ICdwYXBlcicgfHwgaXRlcmF0b3IudHlwZSA9PT0gJ3BsYXN0aWMnKSkge1xuICAgICAgICAgICAgICAgIHllbGxvdyArPSBpdGVyYXRvci53ZWlnaHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihpdGVyYXRvci50eXBlID09PSAnb3JnYW5pYycpIHtcbiAgICAgICAgICAgICAgICBncmVlbiArPSBpdGVyYXRvci53ZWlnaHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihpdGVyYXRvci50eXBlID09PSAnZ2xhc3MnICYmIGl0ZXJhdG9yLnJlY3ljbGFibGUpIHtcbiAgICAgICAgICAgICAgICBnbGFzcyArPSBpdGVyYXRvci53ZWlnaHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHtncmV5LCB5ZWxsb3csIGdyZWVuLCBnbGFzc307XG4gICAgfVxufSIsImltcG9ydCB7IFNtYXJ0QmluIH0gZnJvbSBcIi4vU21hcnRCaW5cIjtcblxuXG5leHBvcnQgY2xhc3MgVmlld0JpbiB7XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNlbGVjdG9yIFPDqWzDqWN0ZXVyIGNzcyBkZSBsJ8OpbMOpbWVudCBvw7kgbWV0dHJlIGwnYWZmaWNoYWdlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHZvbHVtZSBsZSB2b2x1bWUgZGUgbGEgcG91YmVsbGVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gY29sb3IgIGxhIGNvdWxldXIgZGUgbGEgcG91YmVsbGVcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihzZWxlY3Rvciwgdm9sdW1lLCBjb2xvcikge1xuICAgICAgICB0aGlzLnNlbGVjdG9yID0gc2VsZWN0b3I7XG4gICAgICAgIHRoaXMuc21hcnRCaW4gPSBuZXcgU21hcnRCaW4odm9sdW1lLCBjb2xvcik7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIGfDqW7DqHJlIGxlIGh0bWwgZGUgbGEgcG91YmVsbGUgZW4gcHJvcHJpw6l0w6lcbiAgICAgKiBAcmV0dXJucyB7RWxlbWVudH0gTCfDqWzDqW1lbnQgaHRtbCBnw6luw6lyw6lcbiAgICAgKi9cbiAgICBkcmF3KCkge1xuICAgICAgICBsZXQgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0aGlzLnNlbGVjdG9yKTtcbiAgICAgICAgdGFyZ2V0LmlubmVySFRNTCA9ICcnO1xuXG4gICAgICAgIGxldCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnc21hcnQtYmluJyk7XG4gICAgICAgIGVsZW1lbnQudGV4dENvbnRlbnQgPSAnQWN0dWFsIFZvbHVtZSA6ICcrdGhpcy5zbWFydEJpbi50b3RhbFZvbHVtZSgpXG4gICAgICAgIHRoaXMuZHJhd1dlaWdodChlbGVtZW50KTtcbiAgICAgICAgZWxlbWVudC5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSB0aGlzLmNvbG9yO1xuICAgICAgICB0YXJnZXQuYXBwZW5kQ2hpbGQoZWxlbWVudCk7XG5cbiAgICAgICAgcmV0dXJuIGVsZW1lbnQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIGfDqW7DqHJlIGxlIGh0bWwgcmVwcsOpc2VudGFudCBsZSBwb2lkIGRlcyBkw6ljaGV0c1xuICAgICAqIEBwYXJhbSB7RWxlbWVudH0gZWxlbWVudCBMJ8OpbMOpbWVudCBodG1sIG/DuSBhcHBlbmQgbGUgcG9pZCBkZXMgZMOpY2hldHNcbiAgICAgKi9cbiAgICBkcmF3V2VpZ2h0KGVsZW1lbnQpIHtcbiAgICAgICAgbGV0IHRvdGFsID0gdGhpcy5zbWFydEJpbi50b3RhbCgpO1xuICAgICAgICBsZXQgcEdyZXkgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBHcmV5LnRleHRDb250ZW50ID0gJ0dyZXkgOiAnKyB0b3RhbC5ncmV5ICsgJyBncic7XG4gICAgICAgIGVsZW1lbnQuYXBwZW5kQ2hpbGQocEdyZXkpO1xuXG4gICAgICAgIGxldCBwR2xhc3MgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBHbGFzcy50ZXh0Q29udGVudCA9ICdHbGFzcyA6ICcrIHRvdGFsLmdsYXNzICsgJyBncic7XG4gICAgICAgIGVsZW1lbnQuYXBwZW5kQ2hpbGQocEdsYXNzKTtcblxuICAgICAgICBsZXQgcEdyZWVuID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgICAgICBwR3JlZW4udGV4dENvbnRlbnQgPSAnR3JlZW4gOiAnKyB0b3RhbC5ncmVlbiArICcgZ3InO1xuICAgICAgICBlbGVtZW50LmFwcGVuZENoaWxkKHBHcmVlbik7XG5cbiAgICAgICAgbGV0IHBZZWxsb3cgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBZZWxsb3cudGV4dENvbnRlbnQgPSAnWWVsbG93IDogJysgdG90YWwueWVsbG93ICsgJyBncic7XG4gICAgICAgIGVsZW1lbnQuYXBwZW5kQ2hpbGQocFllbGxvdyk7XG4gICAgfVxuXG59IiwiZXhwb3J0IGNsYXNzIFdhc3RlIHtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gd2VpZ2h0IGxlIHBvaWQgZHUgZMOpY2hldFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB2b2x1bWUgbGUgdm9sdW1lIHF1ZSBwcmVuZCBsZSBkw6ljaGV0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHR5cGUgbGEgbWF0acOocmUgZHUgZMOpY2hldCAocGxhc3RpYywgZ2xhc3MsIG9yZ2FuaWMsIG90aGVyLCBwYXBlcilcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IHJlY3ljbGFibGUgc2kgbGUgZMOpY2hldCBlc3QgcmVjeWNsYWJsZSBvdSBwYXNcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3Rvcih3ZWlnaHQsIHZvbHVtZSwgdHlwZSwgcmVjeWNsYWJsZSkge1xuICAgICAgICB0aGlzLndlaWdodCA9IHdlaWdodDtcbiAgICAgICAgdGhpcy50eXBlID0gdHlwZTtcbiAgICAgICAgdGhpcy52b2x1bWUgPSB2b2x1bWU7XG4gICAgICAgIHRoaXMucmVjeWNsYWJsZSA9IHJlY3ljbGFibGU7XG4gICAgfVxufSJdLCJzb3VyY2VSb290IjoiIn0=