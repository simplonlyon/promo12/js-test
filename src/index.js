import { ViewBin } from "./recycle/ViewBin";
import { Waste } from "./recycle/Waste";

// Je créer une viewBin qui elle même créer une SmartBin
let view = new ViewBin('#target', 10, 'blue');

view.draw();

console.log("coucou1");

// Récupérer les valeurs des inputs de notre form
let inputType = document.querySelector("#waste_type");
let inputVolume = document.querySelector("#waste_volume");
let inputRecyclable = document.querySelector("#waste_recyclable");
let inputWeight = document.querySelector("#waste_weight");
let buttonSubmit = document.querySelector("#submit_waste");

console.log("coucou2");

buttonSubmit.addEventListener("click", function(){
    //créer un new waste, avec les valeurs qu'on récupère au click
    let newWaste = new Waste(
                        parseInt(inputWeight.value), 
                        parseInt(inputVolume.value), 
                        inputType.value, 
                        inputRecyclable.value);
    console.log(newWaste);
    
    view.smartBin.add(newWaste);
    
    console.log(view.smartBin.content);

    view.draw();
});